var player, buttons;
const SIZE = 3;

function markCell(id) {
  let addToArray = function(horizontalCoordinate, verticalCoordinate, returnedArray) {
    let button = buttons[horizontalCoordinate][verticalCoordinate], returnedAddToArray = button.innerHTML.trim() === symbol;
    if (returnedAddToArray) {
      returnedArray.push(button);
    }
    return returnedAddToArray;
  }
  let lineAndColCheck = function(coordinateSelector, addValue) {
    let coordinates = [i, j], select = coordinateSelector ? 0 : 1, returned = true, returnedArray = [];
    while (returned && (coordinates[select] += addValue) >= 0 && coordinates[select] < SIZE) {
      returned = addToArray(coordinates[0], coordinates[1], returnedArray);
    }
    return returned ? returnedArray : false;
  }
  let diagonalsCheck = function(horizontalAddValue, verticalAddValue) {
    let iCopy = i, jCopy = j, half = Math.trunc(SIZE / 2), returned = true, returnedArray = [],
    exceptions = (i === j && horizontalAddValue !== verticalAddValue) || (i !== j && horizontalAddValue === verticalAddValue);
    if (((i === half || j === half) && i !== j) || (exceptions && (i !== half || j !== half))) {
      return false;
    }
    while (returned && (iCopy += horizontalAddValue) >= 0 && (jCopy += verticalAddValue) >= 0 && iCopy < SIZE && jCopy < SIZE) {
      returned = addToArray(iCopy, jCopy, returnedArray);
    }
    return returned  ? returnedArray : false;
  }
  let endState = function(message) {
      document.getElementById('showTheWinner').innerHTML = message;
      document.getElementById('showTheWinner').innerHTML += '<button onClick="startGame();"> NEW GAME </button>';
  }
  let showTheWinner = function() {
    for (const buttonLine of buttons) {
      for (const buttonCell of buttonLine) {
        buttonCell.setAttribute('onclick', '');
      }
    }
    for (const button of returnedArray1.concat(returnedArray2)) {
      document.getElementById(button.id).setAttribute('style', 'background-color: #4CAF50');
    }
    buttons[i][j].setAttribute('style', 'background-color: #4CAF50');
    winner = true;
    endState('Player with "' + symbol + '" is the winner&nbsp');
  }

  document.getElementById("showTheWinner").innerHTML = '';
  player = !player;
  let symbol;
  if (player) {
    symbol = "x";
  } else {
    symbol = "0";
  }
  let i = Number.parseInt(id.charAt(3)), j = Number.parseInt(id.charAt(4)), cell = document.getElementById(id);
  cell.innerHTML = symbol;
  cell.setAttribute('style', 'color: red');
  cell.setAttribute('onclick', '');
  let returnedArray1 = lineAndColCheck(true, -1), returnedArray2 = lineAndColCheck(true, 1), winner = false, message;
  if (returnedArray1 && returnedArray2) {
    showTheWinner();
  }
  if ((returnedArray1 = lineAndColCheck(false, -1)) && (returnedArray2 = lineAndColCheck(false, 1))) {
    showTheWinner();
  }
  if ((returnedArray1 = diagonalsCheck(-1, -1)) && (returnedArray2 = diagonalsCheck(1, 1))) {
    showTheWinner();
  }
  if ((returnedArray1 = diagonalsCheck(-1, 1)) && (returnedArray2 = diagonalsCheck(1, -1))) {
    showTheWinner();
  }
  if (!winner) {
    let allCellsMarked = true;
    for (const buttonLine of buttons) {
      for (const buttonCell of buttonLine) {
        if (buttonCell.innerHTML != 'x' && buttonCell.innerHTML != '0') {
          allCellsMarked = false;
          break;
        }
      }
    }
    if (allCellsMarked) {
      endState('The game ended in a draw&nbsp');
    }
  }
}

function startGame() {
  document.getElementById("legend").innerHTML = 'Click the cell where you move';
  document.getElementById("buttons").innerHTML = '';
  for (let i = 0; i < SIZE; ++i) {
    for (let j = 0; j < SIZE; ++j) {
      document.getElementById("buttons").innerHTML += '<button id="pos' + i + j +'" style = "color: transparent" onClick = "markCell(id);"> v </button>';
    }
    document.getElementById("buttons").innerHTML += '<br>';
  }
  player = false;
  buttons = Array(SIZE);
  for (let i = 0; i < SIZE; ++i) {
    buttons[i] = new Array(SIZE);
    for (let j = 0; j < SIZE; ++j) {
      buttons[i][j] = document.getElementById("pos" + i + j);
    }
  }
  document.getElementById("showTheWinner").innerHTML = '*You could chose the symbol for the player which moves first to be "0" ("x" is the default symbol) here:';
  document.getElementById("showTheWinner").innerHTML += '<br><button onClick="player = true;"> CHOSE "0" FOR THE FIRST PLAYER`S SYMBOL </button>';
}
